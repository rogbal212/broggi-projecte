<?php

namespace App\Http\Controllers\Api;

use App\Models\Recurs;
use App\Clases\Utilitat;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\RecursResource;
use Illuminate\Database\QueryException;

class RecursController extends Controller
{
    public function index()
    {

<<<<<<< HEAD
        // try
        // {
        //     //guardamos las ciudades a una variable
        //     $recursos = Recurs::all();
        //     $respuesta = (RecursResource::collection($recursos))->response()->setStatusCode(201);
        // } catch (QueryException $e) {
        //     $mensaje = Utilitats::errorMessage($e);
        //     $respuesta = response()->json(['error'=>$mensaje], 403);
        // }

        // //devuelve un json con todas las ciudades
        // return $respuesta;
        $recursos = Recurs::with('tipus_recurs', 'usuaris')->get();
        return RecursResource::collection($recursos);
=======
        try
        {
            //guardamos las ciudades a una variable
            $recursos = Recurs::all();
            $respuesta = (new RecursResource($recursos))->response()->setStatusCode(201);
        } catch (QueryException $e) {
            $mensaje = Utilitats::errorMessage($e);
            $respuesta = response()->json(['error'=>$mensaje], 403);
        }

        //devuelve un json con todas las ciudades
        return $respuesta;
>>>>>>> master
    }

    public function store(Request $request)
    {
        $recurs = new Recurs();
        $recurs->codi = $request->input('codi');
        $recurs->tipus_recurs_id = $request->input('tipus_recurs_id');
        $recurs->id_usuario = $request->input('id_usuario');

        try
        {
            $recurs->save();
<<<<<<< HEAD
            $respuesta = (new RecursResource($recurs))->response()->setStatusCode(201);
=======
            $respuesta = (new RecursResource($recursos))->response()->setStatusCode(201);
>>>>>>> master
        }
        catch (QueryException $e)
        {
            $mensaje = Utilitats::errorMessage($e);
            $respuesta = response()->json(['error'=>$mensaje], 400);
        }

        return $respuesta;
    }

    public function show($id)
    {
        try
        {
            //para debolver el recurso con el tipo_recurs y usuario
            $recurs = Recurs::with('tipus_recurs', 'usuaris')->find($id);
            if($recurs == null)
            {
                $respuesta = response()->json(['error'=> "No s'ha trobat el recurs"], 404);
            }
<<<<<<< HEAD
            $respuesta = (new RecursResource($recurs))->response()->setStatusCode(201);
=======
            $respuesta = (new RecursResource($recursos))->response()->setStatusCode(201);
>>>>>>> master
        }
        catch (QueryException $e)
        {
            $mensaje = Utilitats::errorMessage($e);
            $respuesta = response()->json(['error'=>$mensaje], 403);
        }

        return $respuesta;
    }

    public function update(Request $request, $id)
    {
<<<<<<< HEAD
        // $recurs->codi = $request->input('codi');
        $recurs = Recurs::find($id);

        if($recurs==null){
            $respuesta = response()->json(['error'=>"registro no encontrado"], 404);
        }
        else{
            $recurs->id = $request->input('id');
            $recurs->codi = $request->input('codi');
            $recurs->tipus_recurs_id = $request->input('tipus_recurs_id');
            $recurs->id_usuario = $request->input('id_usuario');
        }
        try
        {
            $recurs->save();
            $respuesta = (new RecursResource($recurs))->response()->setStatusCode(200);
=======
        $recurs->codi = $request->input('codi');

        try
        {
            $recurs->save();
            $respuesta = (new RecursResource($recursos))->response()->setStatusCode(200);
>>>>>>> master
        }
        catch (QueryException $e)
        {
            $mensaje = Utilitats::errorMessage($e);
            $respuesta = response()->json(['error'=>$mensaje], 400);
        }

        return $respuesta;
    }

    public function destroy($id)
    {
<<<<<<< HEAD
        $recurs = Recurs::find($id);

        if($recurs==null){
            $respuesta = response()->json(['error'=>"registro no encontrado"], 404);
        }
        try
        {
            $recurs->delete();
            $respuesta = (new RecursResource($recurs))->response()->setStatusCode(200);
=======
        try
        {
            $recurs->destroy();
            $respuesta = (new RecursResource($recursos))->response()->setStatusCode(200);
>>>>>>> master
        }
        catch (QueryException $e)
        {
            $mensaje = Utilitats::errorMessage($e);
            $respuesta = response()->json(['error'=>$mensaje], 400);
        }

        return $respuesta;
    }
}