<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Incidencia;
use Illuminate\Http\Request;

use App\Clases\Utilitat;
use App\Http\Resources\IncidenciaResource;
use App\Http\Resources\MunicipiResource;

class IncidenciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $incidencies = Incidencia::all();
        
        return new IncidenciaResource($incidencies);
        //return new IncidenciaNomMunicipiResource($incidencies);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $incidencia = new Incidencia();

        $incidencia->id_incidencia = $request->input('id_incidencia');
        $incidencia->num_incidencia = $request->input('num_incidencia');
        $incidencia->telefon_alertant = $request->input('telefon_alertant');
        $incidencia->data = $request->input('date');
        $incidencia->hora = $request->input('hora');
        $incidencia->adreca = $request->input('adreca');
        $incidencia->complement_adreca = $request->input('complement_adreca');
        $incidencia->descripcio = $request->input('descripcio');
        $incidencia->municipis_id = $request->input('municipis_id');
        $incidencia->tipus_incident_id = $request->input('tipus_incident_id');
        $incidencia->estats_incidencia_id = $request->input('estats_incidencia_id');
        $incidencia->tipus_alertant_id = $request->input('tipus_alertant_id');
        $incidencia->alertants_id = $request->input('alertants_id');

        try {
            $incidencia->save();
            $respuesta = (new IncidenciaResource($incidencia))->response()->setStatusCode(201);
        } catch (QueryException $e) {
            $mensaje = Utilitat::errorMessage($e);
            $respuesta = response()->json(["error"=> $mensaje ], 400);
        }

        return $respuesta;
        

        //$table->foreign('user_id')
        //->references('id')
        //->on('users')
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Incidencia  $incidencia
     * @return \Illuminate\Http\Response
     */
    public function show(Incidencia $incidencia)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Incidencia  $incidencia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Incidencia $incidencia)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Incidencia  $incidencia
     * @return \Illuminate\Http\Response
     */
    public function destroy(Incidencia $incidencia)
    {
        try {
            $incidencia->destroy();
            $respuesta = (new IncidenciaResource($recursos))->response()->setStatusCode(200);
        }
        catch (QueryException $e)
        {
            $mensaje = Utilitats::errorMessage($e);
            $respuesta = response()->json(['error'=>$mensaje], 400);
        }

        return $respuesta;
        //$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
    }

    public function mostrarFormulari()
    {
        return view('formulariIncidencies');
    }
}
