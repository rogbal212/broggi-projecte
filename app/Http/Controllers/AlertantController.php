<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Alertant;
use App\Models\Municipi;
use App\Models\Tipo_alertant;
use Illuminate\Http\Request;
use App\Clases\Utilitat;

use App\Http\Resources\AlertantResource;
use Illuminate\Database\QueryException;

class AlertantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        // if($request->has('search')){
        //     $search = $request->input('search');
        //     $alertants = Alertant::where('nom', 'like', '%'.$search.'%')
        //                         ->orderby('nom')
        //                         ->paginate(10);
        // }
        // else{
        //     $search = ' ';
        //     $alertants = Alertant::orderby('nom')->paginate(10);
        // }

        // $datos['alertants'] = $alertants;
        // $datos['search'] = $search;

        // return view('Alertant.alertants', $datos);
        return \view('Alertant.alertants');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $municipis = Municipi::all();
        $tipo_alertant = Tipo_alertant::all();

        $data['municipis'] = $municipis;
        $data['tipo_alertant'] = $tipo_alertant;
        return view('Alertant.create', $data);
    }

    public function store(Request $request)
    {
        $alertant = new Alertant();
        $ultimAlertant = Alertant::select('id')->orderBy('id','desc')->first();

        $alertant->id = $ultimAlertant->id+1;
        $alertant->nom = $request->input('nom');
        $alertant->cognoms = $request->input('cognoms');
        $alertant->adreca = $request->input('adreca');
        $alertant->municipis_id = $request->input('municipi');
        $alertant->telefon = $request->input('telef');
        $alertant->tipus_alertant_id = $request->input('tipusAlertant');

        try{
        $alertant->save();
        }
        catch(QueryException $e){
            $error=Utilitat::errorMessage($e);
            $request->session()->flash('error', $error);
            return redirect()->action('AlertantController@create')->withInput();
        }

        return redirect()->action('AlertantController@index')->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Alertant  $alertant
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $alertant = Alertant::find($id);

        return new AlertantResource($alertant);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Alertant  $alertant
     * @return \Illuminate\Http\Response
     */
    public function edit(Alertant $alertant)
    {
        $municipis = Municipi::all();
        $tipo_alertant = Tipo_alertant::all();

        $data['municipis'] = $municipis;
        $data['tipo_alertant'] = $tipo_alertant;
        $data['alertant'] = $alertant;
        return view('Alertant.edit', $data);

    }
    public function update(Request $request, Alertant $alertant)
    {
        $alertant->id = $alertant->id;
        $alertant->nom = $request->input('nom');
        $alertant->cognoms = $request->input('cognoms');
        $alertant->adreca = $request->input('adreca');
        $alertant->municipis_id = $request->input('municipi');
        $alertant->telefon = $request->input('telef');
        $alertant->tipus_alertant_id = $request->input('tipusAlertant');


        try{
            $alertant->save();
        }
        catch(QueryException $e){
            $error=Utilitat::errorMessage($e);
            $request->session()->flash('error', $error);
            return redirect()->action('AlertantController@edit')->withInput();
        }

        return redirect()->action('AlertantController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Alertant  $alertant
     * @return \Illuminate\Http\Response
     */
    public function destroy(Alertant $alertant)
    {
        $alertant->delete();

        return redirect()->action('AlertantController@index');
    }
}
