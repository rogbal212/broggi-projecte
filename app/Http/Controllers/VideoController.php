<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Clases\Video;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;


class VideoController extends Controller
{
    public function rellenarVideos()
    {
        $videos = [];

        $video = new video();
        $video->descripcion='Caiguda Per ACR';
        $video->video = 'videos/CLIP_1.mp4';
        array_push($videos, $video);

        $video = new video();
        $video->descripcion='Consciència';
        $video->video = 'videos/CLIP_2.mp4';
        array_push($videos, $video);

        $video = new video();
        $video->descripcion='Valoració respiració';
        $video->video = 'videos/CLIP_3.mp4';
        array_push($videos, $video);

        $video = new video();
        $video->descripcion='RCP 1';
        $video->video = 'videos/CLIP_4.mp4';
        array_push($videos, $video);

        $video = new video();
        $video->descripcion='RCP 4';
        $video->video = 'videos/CLIP_5.mp4';
        array_push($videos, $video);

        $video = new video();
        $video->descripcion='RCP 2';
        $video->video = 'videos/CLIP_6.mp4';
        array_push($videos, $video);

        $video = new video();
        $video->descripcion='RCP 6';
        $video->video = 'videos/CLIP_7.mp4';
        array_push($videos, $video);


        return $videos;
    }

    public function index(Request $request)
    {
        $videos = $this->rellenarVideos();
        $request->session()->put('videos',$videos);

        $datos['videos'] = $videos;
        $datos['titulo'] = 'VIDEOS';
        return view('videos', $datos);
    }

}
