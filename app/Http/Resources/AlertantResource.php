<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\MunicipiResource;
use App\Http\Resources\TipoAlertantResource;

class AlertantResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return[
            'id'=> $this->id ,
            'nom'=> $this->nom,
            'cognoms'=> $this->cognoms,
            'adreca'=> $this->adreca,
            'municipis_id'=>$this->municipis_id,
            'telefon'=> $this->telefon,
            'tipus_alertant_id'=>$this->tipus_alertant_id,
            'municipi'=> new MunicipiResource($this->municipi),
            'tipoAlertant'=> new TipoAlertantResource($this->tipo_alertant)
        ];
    }
}