<?php

Route::get('/', function () {
    return view('index');
});


Route::get('/login', 'Auth\LoginController@showLogin')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::get('/logout','Auth\LoginController@logout')->name('logout');
Route::get('/home','HomeController@index');
Route::get('/recursos','RecursController@index')->name('recursos');
Route::resource('/usuari', 'UsuariController');
// Route::get('/novaIncidencia', 'IncidenciaController@mostrarFormulari');

Route::get('/taulaIncidencia', 'IncidenciaController@mostrarTaula');
Route::get('/taulaGeneral', 'IncidenciaController@mostrarTaulaGeneral');
Route::get('/alertants','AlertantController@index')->name('alertants');
// //todas las rutas que se necessite autorizacion para entrar
// Route::group(['middleware' => ['auth']], function () {
//     Route::get('/home','HomeController@index');
// });
<<<<<<< HEAD

Route::get('/videos', 'VideoController@index')->name('videos');
=======
>>>>>>> master
