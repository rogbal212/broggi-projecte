@extends('templates.main')

@section('titulo')
    Alertant
@endsection

@section('principal')

    @include('partial.errores')

    <div class="card mt-2 border-primary " style="background-color: #FFC4C2;  border-color: #8C031C !important">
        <div class="card-header text-white" style="background-color: #8C031C">
            Alertant
        </div>
        <div class="card-body">
            <form action="{{action('AlertantController@store')}}" method="post" >
                @csrf

                <div class="form-group row">
                    <label for="txtNom" class="col-sm-2 col-form-label">Nom</label>
                    <div class="col-sm-10">
                        <input type="text" name="nom" id="txtNom" class="form-control" placeholder="Nom alertant" aria-describedby="helpID" value="{{old('nom')}}">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="txtCognoms" class="col-sm-2 col-form-label">Cognoms</label>
                    <div class="col-sm-10">
                        <input type="text" name="cognoms" id="txtCognoms" class="form-control" placeholder="Cognoms del alertant" aria-describedby="helpID" value="{{old('cognoms')}}">
                    </div>
                </div>

                <div class="form-group row">
                        <label for="txtAdreca" class="col-sm-2 col-form-label">Adreça</label>
                        <div class="col-sm-10">
                            <input type="text" name="adreca" id="txtAdreca" class="form-control" placeholder="Adreça alertant" aria-describedby="helpID" value="{{old('adreca')}}">
                        </div>
                    </div>

                <div class="form-group row">
                    <label for="cbxMunicipis" class="col-sm-2 col-form-label">Municipis</label>
                    <div class="col-sm-10">
                        <select name="municipi" id="cbxMunicipis" class="custom-select">
                            @foreach ($municipis as $municipi)
                                @if ($municipi->id==old('municipi'))
                                    <option value="{{$municipi->id}}" selected>{{$municipi->nom}}</option>
                                @else
                                    <option value="{{$municipi->id}}">{{$municipi->nom}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="txtTelef" class="col-sm-2 col-form-label">Telèfon</label>
                    <div class="col-sm-10">
                        <input type="text" name="telef" id="txtTelef" class="form-control" placeholder="Telèfon alertant" aria-describedby="helpID" value="{{old('telef')}}">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="cbxTipus" class="col-sm-2 col-form-label">Tipus Alertant</label>
                    <div class="col-sm-10">
                        <select name="tipusAlertant" id="cbxTipus" class="custom-select">
                            @foreach ($tipo_alertant as $tipo)
                                @if ($tipo->id==old('tipusAlertant'))
                                    <option value="{{$tipo->id}}" selected>{{$tipo->tipus}}</option>
                                @else
                                    <option value="{{$tipo->id}}" >{{$tipo->tipus}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-11 ml-1">
                        <button type="submit" class="btn btn-primary">ACEPTAR</button>
                        <a name="" id="" class="btn btn-secondary" href="{{url('/alertants')}}" role="button">CANCELAR</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
