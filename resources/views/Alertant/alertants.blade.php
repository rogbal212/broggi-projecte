@extends('templates.main')

@section('titulo')
Alertants
@endsection

@section('menu')

{{-- lo que s'afegeix a la navBar quan et registres --}}
    <li class="nav-item ">
        <a class="nav-link" href="{{ route('recursos')}}"> RECURSOS </a>
    </li>
    <li class="nav-item ">
        <a class="nav-link" href="{{ route('alertants')}}"> ALERTANTS </a>
    </li>
    <li class="nav-item ">
        <a class="nav-link" href=""> LLISTA INCIDENCIES </a>
    </li>
@endsection

@section('principal')
<alertant></alertant>

@endsection

<style>
    .pagination{
        margin-left: 2rem !important;
    }
    .page-link{
        border-color: #8C031C !important;
        background-color: white !important;
        color:black !important;

    }
    .page-item.active .page-link
    {
        background-color: #8C031C !important;
        color:white !important;
    }
</style>
