@extends('templates.main')

@section('titulo')
    <link rel="stylesheet" href=" {{ asset('css/app.css') }} " >
    <script src="{{ asset('js/app.js') }} "></script>
    Nova Incidència

@endsection

@section('menu')

    <li class="nav-item ">
        <a class="nav-link" href="{{ route('recursos')}}"> RECURSOS </a>
    </li>
    <li class="nav-item ">
        <a class="nav-link" href="{{ route('alertants')}}"> ALERTANTS </a>
    </li>
    <li class="nav-item ">
        <a class="nav-link" href=""> LLISTA ALERTES </a>
    </li>
    <li class="nav-item ">
        <a class="nav-link" href="{{ route('videos') }}"> VIDEOS </a>
    </li>

@endsection

@section('principal')

    <div class="card ml-5 mr-5 mt-5 border-primary">
        <div class="rounded" style="background-color: #EADAB6;" >
            <embed src="storage/videos/RCP_ADULTS.pdf" type="application/pdf" width="100%" height="400px" navpanes='1'/>
            <h5 class="card-title text-center">RCP ADULTS</h5>
        </div>
    </div>

    <div class="container-fluid ">
        <div class="card-deck">
            @foreach ($videos as $video)
                <div class="card-lg-5 ml-5 mr-7 mt-5 mx-auto">
                    <div class="card border-primary">
                        <div class="fa fa-pause rounded" style="background-color: #EADAB6;" muted>
                            <iframe class="embed-responsive-item" src="{{ asset('storage/' . $video->video)}}" type="video/mp4" frameborder="0"> </iframe>
                            <h5 class="card-title text-center" >{{ $video->descripcion }}</h5>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

@endsection
